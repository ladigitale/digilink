<?php

session_start();

$env = '.env';
$donneesEnv = '';
if ((isset($_SESSION['domaineShlink']) && $_SESSION['domaineShlink'] !== '') || (isset($_SESSION['cleApiShlink']) && $_SESSION['cleApiShlink'] !== '')) {
	if (isset($_SESSION['domaineShlink']) && $_SESSION['domaineShlink'] !== '') {
		$shlink_domaine = $_SESSION['domaineShlink'];
	}
	if (isset($_SESSION['cleApiShlink']) && $_SESSION['cleApiShlink'] !== '') {
		$shlink_api_key = $_SESSION['cleApiShlink'];
	}
} else if (file_exists($env)) {
	$donneesEnv = explode("\n", file_get_contents($env));
	foreach ($donneesEnv as $ligne) {
		preg_match('/([^#]+)\=(.*)/', $ligne, $matches);
		if (isset($matches[2])) {
			putenv(trim($ligne));
		}
	}
	$shlink_domaine = getenv('SHLINK_DOMAIN');
	$shlink_api_key = getenv('SHLINK_API_KEY');
	$_SESSION['domaineShlink'] = $shlink_domaine;
	$_SESSION['cleApiShlink'] = $shlink_api_key;
} else {
	$shlink_domaine = '';
	$shlink_api_key = '';
}

if (isset($_SESSION['domainesAutorises']) && $_SESSION['domainesAutorises'] !== '') {
	$domainesAutorises = $_SESSION['domainesAutorises'];
} else if ($donneesEnv !== '') {
	$domainesAutorises = getenv('AUTHORIZED_DOMAINS');
	$_SESSION['domainesAutorises'] = $domainesAutorises;
} else if (file_exists($env)) {
	$donneesEnv = explode("\n", file_get_contents($env));
	foreach ($donneesEnv as $ligne) {
		preg_match('/([^#]+)\=(.*)/', $ligne, $matches);
		if (isset($matches[2])) {
			putenv(trim($ligne));
		}
	}
	$domainesAutorises = getenv('AUTHORIZED_DOMAINS');
	$_SESSION['domainesAutorises'] = $domainesAutorises;
} else {
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: POST');
	header('Access-Control-Max-Age: 1000');
	header('Access-Control-Allow-Headers: Content-Type, X-Requested-With');
	$domainesAutorises = '';
}

if ($domainesAutorises !== '') {
	if ($domainesAutorises === '*') {
		$origine = $domainesAutorises;
	} else {
		$domainesAutorises = explode(',', $domainesAutorises);
		$origine = $_SERVER['SERVER_NAME'];
	}
	if ($origine === '*' || in_array($origine, $domainesAutorises, true)) {
		header('Access-Control-Allow-Origin: $origine');
		header('Access-Control-Allow-Methods: POST');
		header('Access-Control-Max-Age: 1000');
		header('Access-Control-Allow-Headers: Content-Type, X-Requested-With');
	} else {
		header('Location: /');
		exit();
	}
}

require_once('./vendor/autoload.php');

use SimpleCaptcha\Builder;

$_POST = json_decode(file_get_contents('php://input'), true);

if (!empty($_POST['lien'])) {
	$lien = $_POST['lien'];
	$captcha = Builder::create();
	if (isset($_SESSION['phrase']) && $captcha->compare($_SESSION['phrase'], $_POST['captcha'])) {
		$suffixe = $_POST['suffixe'];
		if ($suffixe !== '') {
			$donnees = 'longUrl=' . $lien . '&customSlug=' . $suffixe . '&findIfExists=true';
		} else {
			$donnees = 'longUrl=' . $lien . '&findIfExists=true';
		}
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, 'https://' . $shlink_domaine . '/rest/v2/short-urls');
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $donnees);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'X-Api-Key: ' . $shlink_api_key));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$reponse = curl_exec($curl);
		if ($reponse === false) {
			echo 'erreur';
		} else {
			echo $reponse;
		}
		curl_close($curl);
	} else {
		echo 'erreur_captcha';
	}
	exit();
} else {
	header('Location: /');
	exit();
}

?>
