# Digilink

Digilink est une interface graphique simple pour générer des liens raccourcis et des codes QR sur un serveur Shlink (https://github.com/shlinkio/shlink).

Elle est publiée sous licence GNU AGPLv3.
Sauf la fonte Mona Sans Expanded (Sil Open Font Licence 1.1)

### Variables d'environnement (fichier .env à créer à la racine du dossier)
```
AUTHORIZED_DOMAINS (* ou liste des domaines autorisés pour les requêtes POST et l'API, séparés par une virgule)
SHLINK_API_KEY (clé API Shlink)
SHLINK_DOMAIN (domaine pour les liens raccourcis Shlink)
```

### Serveur Shlink nécessaire pour l'API
Le projet nécessite une instance opérationnelle de Shlink.

### Démo
https://ladigitale.dev/digilink/

### Soutien
Open Collective : https://opencollective.com/ladigitale

Liberapay : https://liberapay.com/ladigitale/

