<?php

session_start();

$env = '.env';
$donneesEnv = '';
if ((isset($_SESSION['domaineShlink']) && $_SESSION['domaineShlink'] !== '') || (isset($_SESSION['cleApiShlink']) && $_SESSION['cleApiShlink'] !== '')) {
	if (isset($_SESSION['domaineShlink']) && $_SESSION['domaineShlink'] !== '') {
		$shlink_domaine = $_SESSION['domaineShlink'];
	}
	if (isset($_SESSION['cleApiShlink']) && $_SESSION['cleApiShlink'] !== '') {
		$shlink_api_key = $_SESSION['cleApiShlink'];
	}
} else if (file_exists($env)) {
	$donneesEnv = explode("\n", file_get_contents($env));
	foreach ($donneesEnv as $ligne) {
		preg_match('/([^#]+)\=(.*)/', $ligne, $matches);
		if (isset($matches[2])) {
			putenv(trim($ligne));
		}
	}
	$shlink_domaine = getenv('SHLINK_DOMAIN');
	$shlink_api_key = getenv('SHLINK_API_KEY');
	$_SESSION['domaineShlink'] = $shlink_domaine;
	$_SESSION['cleApiShlink'] = $shlink_api_key;
}

if (isset($_SESSION['domainesAutorises']) && $_SESSION['domainesAutorises'] !== '') {
	$domainesAutorises = $_SESSION['domainesAutorises'];
} else if ($donneesEnv !== '') {
	$domainesAutorises = getenv('AUTHORIZED_DOMAINS');
	$_SESSION['domainesAutorises'] = $domainesAutorises;
} else if (file_exists($env)) {
	$donneesEnv = explode("\n", file_get_contents($env));
	foreach ($donneesEnv as $ligne) {
		preg_match('/([^#]+)\=(.*)/', $ligne, $matches);
		if (isset($matches[2])) {
			putenv(trim($ligne));
		}
	}
	$domainesAutorises = getenv('AUTHORIZED_DOMAINS');
	$_SESSION['domainesAutorises'] = $domainesAutorises;
} else {
	echo 'erreur';
	exit();
}

if ($domainesAutorises === '*') {
	$origine = $domainesAutorises;
} else {
	$domainesAutorises = explode(',', $domainesAutorises);
	$origine = $_SERVER['SERVER_NAME'];
}
if ($origine === '*' || in_array($origine, $domainesAutorises, true)) {
	header('Access-Control-Allow-Origin: $origine');
	header('Access-Control-Allow-Methods: POST');
	header('Access-Control-Max-Age: 1000');
	header('Access-Control-Allow-Headers: Content-Type, X-Requested-With');
} else {
	echo 'erreur';
	exit();
}

$_POST = json_decode(file_get_contents('php://input'), true);

if (!empty($_POST['token']) && !empty($_POST['lien'])) {
	$token = $_POST['token'];
	$domaine = $_SERVER['SERVER_NAME'];
	$lien = $_POST['lien'];
	$donnees = array(
		'token' => $token,
		'domaine' => $domaine
	);
	$donnees = http_build_query($donnees);
	$ch = curl_init($lien);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $donnees);
	$resultat = curl_exec($ch);
	if ($resultat === 'non_autorise' || $resultat === 'erreur') {
		echo 'erreur_token';
	} else if ($resultat === 'token_autorise' && !empty($_POST['action'])) {
		$action = $_POST['action'];
		if ($action === 'creer' && !empty($_POST['url'])) {
			$url = $_POST['url'];
			$suffixe = $_POST['suffixe'];
			if ($suffixe !== '') {
				$donnees = 'longUrl=' . $url . '&customSlug=' . $suffixe . '&findIfExists=true';
			} else {
				$donnees = 'longUrl=' . $url . '&findIfExists=true';
			}
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, 'https://' . $shlink_domaine . '/rest/v2/short-urls');
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $donnees);
			curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'X-Api-Key: ' . $shlink_api_key));
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			$reponse = curl_exec($curl);
			if ($reponse === false) {
				echo 'erreur';
			} else {
				echo $reponse;
			}
			curl_close($curl);
		} else if ($action === 'modifier-lien' && !empty($_POST['id']) && !empty($_POST['url'])) {
			$id = $_POST['id'];
			$url = $_POST['url'];
			$donnees = 'longUrl=' . $url;
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, 'https://' . $shlink_domaine . '/rest/v2/short-urls/' . $id);
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $donnees);
			curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'X-Api-Key: ' . $shlink_api_key));
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			$reponse = curl_exec($curl);
			if ($reponse === false) {
				echo 'erreur';
			} else {
				echo $reponse;
			}
			curl_close($curl);
		} else if ($action === 'supprimer' && !empty($_POST['id'])) {
			$id = $_POST['id'];
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, 'https://' . $shlink_domaine . '/rest/v2/short-urls/' . $id);
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
			curl_setopt($curl, CURLOPT_HEADER, true);
			curl_setopt($curl, CURLOPT_NOBODY, true);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'X-Api-Key: ' . $shlink_api_key));
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			$reponse = curl_exec($curl);
			if (curl_errno($curl)) {
				echo 'erreur';
			}
			if (str_contains($reponse, 'HTTP/2 204') || str_contains($reponse, 'HTTP/2 404')) {
				echo 'contenu_supprime';
			} else {
				echo 'erreur';
			}
			curl_close($curl);
		} else {
			echo 'erreur';
		}
	} else {
		echo 'erreur';
	}
	curl_close($ch);
	exit();
} else {
	echo 'erreur';
	exit();
}

?>
