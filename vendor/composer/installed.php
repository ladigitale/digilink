<?php return array(
    'root' => array(
        'name' => '__root__',
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'reference' => NULL,
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'reference' => NULL,
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        's1syphos/php-simple-captcha' => array(
            'pretty_version' => '2.3.0',
            'version' => '2.3.0.0',
            'reference' => '8ceff4950ef75d5deb4a19bbc8e25012818234d9',
            'type' => 'library',
            'install_path' => __DIR__ . '/../s1syphos/php-simple-captcha',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
